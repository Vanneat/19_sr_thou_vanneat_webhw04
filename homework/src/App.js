import logo from './logo.svg';
import './App.css';
import InputComponent from './component/InputComponent';

function App() {
  return (
    <div className="App">
      <InputComponent/>
    </div>
  );
}

export default App;
