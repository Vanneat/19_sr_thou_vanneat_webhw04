import React, { Component } from "react";
import TableComponent from "./TableComponent";
import ButtonComponent from "./ButtonComponent";

export default class InputComponent extends Component {
  constructor() {
    super();
    this.state = {
      student: [
        {
          id: 1,
          stuEmail: "Vanneat@gmail.com",
          stuName: "Vanneat",
          stuAge: "22",
          status: "Pending",
        },
      ],
      newEmail: "",
      newStu: "",
      newAge: "",
      newStatus: "Pending",
    };
  }

  handleOnChangeInputemail = (event) => {
    // console.log("Value2 : ", event.target.value)
    this.setState({
      newEmail: event.target.value,
    });
  };
  handleOnChangeInput = (event) => {
    // console.log("Value1 : ", event.target.value)
    this.setState({
      newStu: event.target.value,
    });
  };
  handleOnChangeInputage = (event) => {
    // console.log("Value3 : ", event.target.value)
    this.setState({
      newAge: event.target.value,
    });
  };

  onSubmit = () => {
    const newObj = {
      id: this.state.student.length + 1,
      stuEmail: this.state.newEmail,
      stuName: this.state.newStu,
      stuAge: this.state.newAge,
      status: "Pending",
    };
    // console.log(newObj)
    this.setState({
      student: [...this.state.student, newObj],
    });
  };

  handleButtonchange=(id)=>{
    // console.log(id)
    // this.setState({

    // })
    this.state.student.map((item) => {
      if(item.id == id ){
        item.status=item.status==="Pending"?"Done":"Pending"
      }
    })

    this.setState({item:this.state.student})
  }

  render() {
    return (
      <div class="">
        <div class="  font-extrabold text-transparent text-4xl bg-clip-text bg-gradient-to-r from-indigo-900 via-red-500 to-pink-500 p-10 ">
          Please Fill Your
          <div class="text-black inline"> Information</div>
        </div>
        <div class="w-[800px] items-center m-auto text-left ">
          <label
            for="input-group-1"
            class="block mb-2 text-xl font-medium text-gray-900 dark:text-white"
          >
            Your Email
          </label>
          <div class="relative mb-6">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-gray-500 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
              </svg>
            </div>
            <input
             onChange={this.handleOnChangeInputemail}
              type="text"
              id="input-group-1"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Vaneat@gmail.com"
            />
          </div>
          <label
            for="website-admin"
            class="block mb-2 text-xl font-medium text-gray-900 dark:text-white"
          >
            Username
          </label>
          <div class="flex">
            <span class="mb-5 inline-flex items-center px-[12px] text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              @
            </span>
            <input
            onChange={this.handleOnChangeInput}
              type="text"
              id="website-admin"
              class="mb-5 rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 "
              placeholder="Vanneat"
            />
          </div>
          <label
            for="website-admin"
            class="block mb-2 text-xl font-medium text-gray-900 dark:text-white "
          >
            Age
          </label>
          <div class="flex">
            <span class="inline-flex items-center px-[10px] text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600 mb-10">
              💔
            </span>
            <input
              onChange={this.handleOnChangeInputage}
              type="text"
              id="website-admin"
              class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 mb-10"
              placeholder="22"
            />
          </div>
        </div>
        {/* ButtonComponent */}

        <ButtonComponent onClick={this.onSubmit} />

        {/* TableComponent */}

        <TableComponent data={this.state.student} handleButtonchange={this.handleButtonchange}/>
      </div>
    );
  }
}
