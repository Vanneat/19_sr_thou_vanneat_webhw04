import React, { Component } from "react";
import Swal from "sweetalert2";

export default class TableComponent extends Component {
  AertStatement = (Alert) =>{
      Swal.fire({
        html:
          'id: '+`${Alert.id}`+'</br>'+
          'Email: '+`${Alert.stuEmail}`+'</br>'+
          'UserName: '+`${Alert.stuName}`+'</br>'+
          'Age: '+`${Alert.stuAge}`+'',
        confirmButtonText:
          '<i class="fa fa-thumbs-up"></i> OK',
      })
  }
  render() {
    return (
      <div class=" w-[800px] items-center m-auto text-left my-10  ">
        <table class="w-full text-sm text-left ">
          <thead class="text-md text-black uppercase ">
            <tr>
              <th scope="col" class="px-6 py-3">
                ID
              </th>
              <th scope="col" class="px-6 py-3">
                Email
              </th>
              <th scope="col" class="px-6 py-3">
                User Name
              </th>
              <th scope="col" class="px-6 py-3">
                Age
              </th>
              <th scope="col" class="px-[130px] py-3">
                            Action
              </th>
            </tr>
          </thead>
          <tbody>
            {this.props.data.map((item) => (
              <tr key={item.id} class={`${item.id % 2 === 0 ? `bg-pink-200 ` : `bg-white`}`}>
                <td class="px-6 py-4">{item.id}</td>
                <td class="px-6 py-4">{ item.stuEmail===""? "null" : item.stuEmail}</td>
                <td class="px-6 py-4">{item.stuName===""? "null" : item.stuName}</td>
                <td class="px-6 py-4">{item.stuAge ===""? "null": item.stuAge}</td>
                <td class="px-6 py-4 ">
                  <button onClick={()=> this.props.handleButtonchange(item.id)} class={`${item.status==="Pending"?`bg-red-900 text-white py-2 px-[40px] border border-red-500 rounded ml-3`:`bg-green-900 text-white py-2 px-[48px] border border-blue-500 rounded ml-3`}`} >
                    {item.status}
                  </button>
                  <button  onClick={()=>this.AertStatement(item)} class=" bg-blue-800 text-white py-2 px-[40px] border border-blue-500 rounded ml-3">
                    Show more
                  </button>
                  
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
