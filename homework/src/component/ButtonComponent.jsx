import React, { Component } from 'react'

export default class ButtonComponent extends Component {
    constructor() {
        super();
        this.state = {
          count: 0,
        };}
  
      updateCount() {
        this.setState((prevState, props) => {
          return { count: prevState.count + 1}
        });
      }
  
  render() {
    return (
    <div>
    <button onClick={this.props.onClick} class=" bg-white bg-transparent hover:bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 font-semibold hover:text-white py-2 px-[100px] border border-blue-500 hover:border-transparent rounded">
          Register
    </button>
    </div>
    )
  }
}

